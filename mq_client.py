#!/usr/bin/env python3
# coding: utf-8

from argparse import(
	RawTextHelpFormatter
)
from markparse import(
	MarkdownArgumentParser as ArgumentParser, UsageAction
)
from os import(
	walk
)
from os.path import(
	isdir, isfile, join
)


class CLIParameters(object):

	def __init__(self):
		super(CLIParameters, self).__init__()
		main_parser = None
		try:
			global_parser = ArgumentParser(
				add_help=False
			)
			global_parser.add_argument(
				"-h", "--help",
				help="""
Write a help message to standard output and exit.
""".strip(),
				action="help"
			)
			global_parser.add_argument(
				"-?", "--usage",
				help="""
Write a usage message to standard output and exit.
""".strip(),
				action=UsageAction
			)

			main_parser = ArgumentParser(
				description="""
Publish messages to, or consume messages from, a queue on an IBM-MQ server.
""".strip(),
				formatter_class=RawTextHelpFormatter,
				parents=[global_parser],
				add_help=False
			)

			common_parser = ArgumentParser(
				add_help=False
			)
			common_parser.add_argument(
				"host",
				help="""
The host of the IBM-MQ server is `«host»`.
""".strip(),
				metavar="«host»",
				type=str,
				action="store"
			)
			common_parser.add_argument(
				"port",
				help="""
The port of the IBM-MQ server is `«port»`.
""".strip(),
				metavar="«port»",
				type=str,
				action="store"
			)
			common_parser.add_argument(
				"manager",
				help="""
The queue manager is `«manager»`.
""".strip(),
				metavar="«manager»",
				type=str,
				action="store"
			)
			common_parser.add_argument(
				"channel",
				help="""
The channel is `«channel»`.
""".strip(),
				metavar="«channel»",
				type=str,
				action="store"
			)
			common_parser.add_argument(
				"queue",
				help="""
The queue is `«queue»`.
""".strip(),
				metavar="«queue»",
				type=str,
				action="store",
			)
			common_parser.add_argument(
				"-v", "--verbose",
				help="""
Write a progress report to standard output.  This argument may be repeated
any number of times to increase verbosity.
""".strip(),
				action="count",
				dest="verbosity",
				required=False,
				default=0
			)

			command_subparsers = main_parser.add_subparsers(
				dest="command"
			)

			consume_parser = command_subparsers.add_parser(
				"consume",
				help="Consume messages.",
				description="""
Consume messages from a queue on an IBM-MQ server.
""".strip(),
				formatter_class=RawTextHelpFormatter,
				parents=[global_parser, common_parser],
				add_help=False
			)

			publish_parser = command_subparsers.add_parser(
				"publish",
				help="Publish messages.",
				description="""
Publish messages to a queue on an IBM-MQ server.
""".strip(),
				formatter_class=RawTextHelpFormatter,
				parents=[global_parser, common_parser],
				add_help=False
			)
			publish_parser.add_argument(
				"paths",
				help="""
Publish the contents of the files at or under the path `«path»`.  This
argument may be used any number of times.
""".strip(),
				metavar="«path»",
				type=str,
				nargs="*"
			)

			main_parser.parse_args(
				namespace=self
			)
		except Exception as exception:
			main_parser.error(str(exception))


class Client(object):

	def __init__(self, host, port, manager_name, channel_name, queue_name, verbosity):
		self.conn_info = "%s(%s)"%(host, port)
		self.manager_name = manager_name
		self.channel_name = channel_name
		self.queue_name = queue_name
		self.verbosity = verbosity

	def __enter__(self):
		from pymqi import(
			Queue,
			connect
		)

		self.manager = connect(self.manager_name, self.channel_name, self.conn_info)
		self.queue = Queue(self.manager, self.queue_name)
		return self

	def __exit__(self, type, value, tb):
		try: self.queue.close()
		except: pass
		try: self.manager.disconnect()
		except: pass

	def consume(self):
		from pymqi import(
			MQMIError
		)
		from pymqi.CMQC import(
			MQCC_FAILED, MQRC_NO_MSG_AVAILABLE
		)

		with self:
			messages = []
			while True:
				try:
					message = self.queue.get().decode("cp500")
				except MQMIError as exception:
					if exception.comp == MQCC_FAILED and exception.reason == MQRC_NO_MSG_AVAILABLE:
						break
				else:
					messages.append(message)
			if self.verbosity >= 1:
				print(
					"Consumed %(count)d %(message)s"%{
						"count": len(messages),
						"message": "message" if len(messages) == 1 else "messages"
					}
				)
			if self.verbosity >= 2:
				if messages:
					for message in messages:
						print("\n%s"%(message))

	def publish(self, *roots):
		paths = []
		for root in roots:
			if isfile(root) and root.lower().endswith(".xml"):
				paths.append(root)
			elif isdir(root):
				for (root, dirs, files) in walk(root):
					for file in files:
						if file.lower().endswith(".xml"):
							paths.append(join(root, file))
		if paths:
			with self:
				for path in paths:
					with open(path, "rb") as handle:
						message=handle.read().decode("utf-8").encode("cp500")
						self.queue.put(message)
		if self.verbosity >= 1:
			print(
				"Published %(count)d %(message)s"%{
					"count": len(paths),
					"message": "message" if len(paths) == 1 else "messages"
				}
			)


if __name__ == "__main__":
	try:
		parameters = CLIParameters()
		client = Client(
			host=parameters.host,
			port=parameters.port,
			manager_name=parameters.manager,
			channel_name=parameters.channel,
			queue_name=parameters.queue,
			verbosity=parameters.verbosity
		)
		if parameters.command == "consume":
			client.consume()
		if parameters.command == "publish":
			client.publish(*parameters.paths)
	except Exception as exception:
		exit(str(exception))
