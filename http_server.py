#!/usr/bin/env python3
# coding: utf-8

from argparse import(
	RawTextHelpFormatter
)
from http.server import(
	HTTPServer, SimpleHTTPRequestHandler
)
from markparse import(
	MarkdownArgumentParser as ArgumentParser
)
from os import(
	getcwd
)
from os.path import(
	join
)
from time import(
	sleep
)


def indent(obj, prefix="    "):
	return "\n".join(
		"%s%s"%(prefix, line)
		for line in str(obj).splitlines()
	)


class CLIParameters(object):

	def __init__(self):
		parser = ArgumentParser(
			description="""
Start an HTTP server on the local host that listens on the TCP port `«port»`
for `GET` requests from HTTP clients on the local host.  For each `GET`
request, sanitise the path of the request URL and try to respond with the HTTP
status code `200 OK` and the content of a file at the sanitised path relative
to the current working directory of the server.

Warning
-------

Each user on the local host with access to the TCP port `«port»` can request
from the server the content of each server-readable file at or beyond the
current working directory of the server.  Therefore, ensure that no
server-readable file at or beyond the current working directory of the server
contains sensitive data.
""".strip(),
			formatter_class=RawTextHelpFormatter,
			add_help=False
		)
		parser.add_argument(
			"port",
			help="""
Listen on TCP port `«port»`.
""".strip(),
			metavar="«port»",
			type=int,
			nargs="?",
			default=6694
		)
		parser.add_argument(
			"-d", "--delay",
			help="""
Add a «delay» second delay when responding to a request.
""".strip(),
			metavar="«delay»",
			type=float,
			dest="delay",
			required=False,
			default=0
		)
		parser.add_argument(
			"-q", "--quiet",
			help="""
Do not log to the standard output.
""".strip(),
			action="store_true",
			dest="quiet",
			default=False
		)
		parser.add_argument(
			"-h", "--help",
			help="""
Write a help message to the standard output.
""".strip(),
			action="help"
		)
		parser.parse_args(
			namespace=self
		)


class Handler(SimpleHTTPRequestHandler):

	def do_GET(self):
		headers = str(self.headers).strip()
		parameters = """
Version: %(version)s
Command: %(command)s
Path: %(path)s
Headers:\n%(headers)s
""".strip()%{
			"version": self.request_version,
			"command": self.command,
			"path": self.path,
			"headers": indent(headers)
		}
		self.server.log("Request\n%s"%(indent(parameters)))
		if self.server.delay:
			self.server.log("Delay: %s seconds"%(self.server.delay))
			sleep(self.server.delay)
		super(Handler, self).do_GET()

	def log_request(self, code="-", size="-"):
		parameters = """
Code: %(code)d
Text: %(text)s
Explanation: %(explanation)s
""".strip()%{
			"code": code,
			"text": self.responses[code][0],
			"explanation": self.responses[code][1]
		}
		self.server.log("Response:\n%s"%(indent(parameters)))


class Server(HTTPServer):

	@staticmethod
	def _log(text):
		print(text)

	@staticmethod
	def _no_log(text):
		pass

	def __init__(self, port, delay=0, quiet=False):
		super(Server, self).__init__(
			("127.0.0.1", port),
			Handler
		)
		self.root = getcwd()
		self.port = port
		self.delay = delay
		if quiet:
			self.log = self._no_log
		else:
			self.log = self._log

	def start(self):
		parameters = """
Root: %(root)s
Port: %(port)s
Delay: %(delay)s seconds
""".strip()%{
				"root": self.root,
				"port": self.port,
				"delay": self.delay
			}
		self.log("Start:\n%s"%(indent(parameters)))
		self.serve_forever()

	def stop(self):
		self.log("Stop")
		self.shutdown()
		self.server_close()


if __name__ == "__main__":
	try:
		parameters = CLIParameters()
		server = Server(
			port=parameters.port,
			delay=parameters.delay,
			quiet=parameters.quiet
		)
		try:
			server.start()
		finally:
			server.stop()
	except KeyboardInterrupt as exception:
		exit(1)
	except Exception as exception:
		exit(str(exception))
