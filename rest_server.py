#!/usr/bin/env python3
# coding: utf-8

from argparse import(
	RawTextHelpFormatter
)
from http.server import(
	HTTPServer, BaseHTTPRequestHandler
)
from json import(
	dumps, loads
)
from markparse import(
	MarkdownArgumentParser as ArgumentParser
)
from time import(
	sleep
)


class CLIParameters(object):

	def __init__(self):
		parser = ArgumentParser(
			description="""
Start an HTTP server on the local host that listens on TCP port `«port»` for
`POST` requests from HTTP clients on the local host.  If a client sends a `POST`
request then the server responds with HTTP status code `200 OK` and a JSON
document that echoes the version, command, path, headers and JSON content of the
request.
""".strip(),
			formatter_class=RawTextHelpFormatter,
			add_help=False
		)
		parser.add_argument(
			"port",
			help="""
Listen on TCP port `«port»`.
""".strip(),
			metavar="«port»",
			type=int,
			nargs="?",
			default=6695
		)
		parser.add_argument(
			"-d", "--delay",
			help="""
Add a delay of «delay» seconds when responding to a request.
""".strip(),
			metavar="«delay»",
			type=float,
			dest="delay",
			required=False,
			default=0
		)
		parser.add_argument(
			"-q", "--quiet",
			help="""
Do not log requests and responses to standard output.
""".strip(),
			action="store_true",
			dest="quiet",
			default=False
		)
		parser.add_argument(
			"-h", "--help",
			help="""
Write a short help message to standard output and exit.
""".strip(),
			action="help"
		)
		parser.parse_args(
			namespace=self
		)


class Handler(BaseHTTPRequestHandler):

	def do_POST(self):
		if self.server.quiet:
			self.log_request = lambda *args,**kwargs : None
			self.log_message = lambda *args,**kwargs : None
		data = {
			"request_version": self.request_version,
			"command": self.command,
			"path": self.path,
			"headers": {},
			"content": {}
		}
		for line in str(self.headers).strip().splitlines():
			(key, value) = line.split(": ", 1)
			data["headers"][key] = value
		content_length = data["headers"].get("Content-Length")
		if content_length:
			data["content"] = loads(self.rfile.read(int(content_length)).decode("utf-8"))

		report = dumps(
			data,
			indent=2
		)

		if self.server.delay:
			sleep(self.server.delay)

		self.send_response(200)
		self.send_header("Content-Type", "application/json; charset=UTF-8")
		self.end_headers()
		self.log_message("\n%s", report)
		self.wfile.write(("%s\n"%(report)).encode("utf-8"))


class Server(HTTPServer):

	def __init__(self, port, delay=0, quiet=False):
		super(Server, self).__init__(
			("127.0.0.1", port),
			Handler
		)
		self.delay = delay
		self.quiet = quiet

	def start(self):
		self.serve_forever()

	def stop(self):
		self.shutdown()
		self.server_close()



if __name__ == "__main__":
	try:
		parameters = CLIParameters()
		server = Server(
			port=parameters.port,
			delay=parameters.delay
		)
		server.start()
	except KeyboardInterrupt:
		print("Received keyboard interrupt")
		server.stop()
