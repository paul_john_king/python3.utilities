<!--
The script `project` generates this file from the project sources, and
overwrites any changes made to it.  In order to permanently change this file,
edit the project sources and call the command `project document`.
-->

python3.utilities – Some utilties written in Python 3

This project comprises

*   `http_server.py` – a file containing a script that starts a crude
    HTTP server that returns the contents of files to `GET` requests,

*   `rest_server.py` – a file containing a script that starts a crude
    HTTP server that returns the request parameters to `POST` requests,

*   `mq_client.py` – a file containing a script that publishes and
    consumes messages to and from an IBM-MQ server,

*   `project` – a file containing a script to manage the project, and

*   `README.md` – a file containining a description of the project.

`http_server.py`
================

Usage
-----

    http_server.py [-d «delay»] [-q] [-h] [«port»]

Start an HTTP server on the local host that listens on the TCP port `«port»`
for `GET` requests from HTTP clients on the local host.  For each `GET`
request, sanitise the path of the request URL and try to respond with the HTTP
status code `200 OK` and the content of a file at the sanitised path relative
to the current working directory of the server.

Warning
-------

Each user on the local host with access to the TCP port `«port»` can request
from the server the content of each server-readable file at or beyond the
current working directory of the server.  Therefore, ensure that no
server-readable file at or beyond the current working directory of the server
contains sensitive data.

Arguments
---------

*   `«port»`
    
    Listen on TCP port `«port»`.
    
    ----------------------------------------------------------------------------
    *   Required: False
    *   Default: `6694`
    ----------------------------------------------------------------------------

*   `-d «delay»`, `--delay «delay»`
    
    Add a delay of «delay» seconds when responding to a request.
    
    ----------------------------------------------------------------------------
    *   Required: False
    ----------------------------------------------------------------------------

*   `-q`, `--quiet`
    
    Do not log requests and responses to standard output.
    
    ----------------------------------------------------------------------------
    *   Required: False
    ----------------------------------------------------------------------------

*   `-h`, `--help`
    
    Write a short help message to standard output and exit.
    
    ----------------------------------------------------------------------------
    *   Required: False
    ----------------------------------------------------------------------------

`rest_server.py`
================

Usage
-----

    rest_server.py [-d «delay»] [-q] [-h] [«port»]

Start an HTTP server on the local host that listens on TCP port `«port»` for
`POST` requests from HTTP clients on the local host.  If a client sends a `POST`
request then the server responds with HTTP status code `200 OK` and a JSON
document that echoes the version, command, path, headers and JSON content of the
request.

Arguments
---------

*   `«port»`
    
    Listen on TCP port `«port»`.
    
    ----------------------------------------------------------------------------
    *   Required: False
    *   Default: `6695`
    ----------------------------------------------------------------------------

*   `-d «delay»`, `--delay «delay»`
    
    Add a delay of «delay» seconds when responding to a request.
    
    ----------------------------------------------------------------------------
    *   Required: False
    ----------------------------------------------------------------------------

*   `-q`, `--quiet`
    
    Do not log requests and responses to standard output.
    
    ----------------------------------------------------------------------------
    *   Required: False
    ----------------------------------------------------------------------------

*   `-h`, `--help`
    
    Write a short help message to standard output and exit.
    
    ----------------------------------------------------------------------------
    *   Required: False
    ----------------------------------------------------------------------------

`mq_client.py`
==============

Usage
-----

    mq_client.py [-h] [-?] consume|publish …

Publish messages to, or consume messages from, a queue on an IBM-MQ server.

Commands
--------

*   `consume`
    
    Consume messages.

*   `publish`
    
    Publish messages.

Arguments
---------

*   `-h`, `--help`
    
    Write a help message to standard output and exit.
    
    ----------------------------------------------------------------------------
    *   Required: False
    ----------------------------------------------------------------------------

*   `-?`, `--usage`
    
    Write a usage message to standard output and exit.
    
    ----------------------------------------------------------------------------
    *   Required: False
    ----------------------------------------------------------------------------

`project`
=========

The command

    project usage

writes a short usage message about `project` to the standard output.

The command

    project help

writes a longer help message about `project` to the standard output.

The command

    project document

writes a description of the project to the file at `README.md` in
[GitLab Flavored Markdown][gfm] format.

References
----------

["GitLab Flavored Markdown"][gfm]

[gfm]: https://docs.gitlab.com/ee/user/markdown.html

Warning
=======

The script `project` generates this file from the project sources, and
overwrites any changes made to it.  In order to permanently change this file,
edit the project sources and call the command `project document`.
